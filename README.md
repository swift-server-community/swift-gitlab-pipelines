# Swift GitLab Pipelines

A shared repository of GitLab CI job definitions to standardize testing and building Swift projects on GitLab.

## Contributing

Have an idea for a pipeline job? [File an issue](https://gitlab.com/swift-server-community/swift-gitlab-pipelines/-/issues/new) pitching your idea!

## License

This project is made available under the MIT license.

See [LICENSE](./LICENSE) for more details.
